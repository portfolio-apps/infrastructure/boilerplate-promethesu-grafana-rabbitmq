#!/bin/bash

if [ -z "$1" ]
then
      echo "ERROR: First argument for APP_ENV is empty"
      exit
fi

if [ -z "$2" ]
then
      echo "ERROR: Second argument for NAMESPACE is empty"
      exit
fi

# if [ -z "$3" ]
# then
#       echo "ERROR: Third argument for TAG is empty"
#       exit
# fi

### Set Environment Variables
set -a # automatically export all variables
source .env.production
set +a

helm -n $2 upgrade -i --debug --wait --atomic \
server ./k8s/helm

echo ""
echo ""

kubectl -n $2 get all