#!/bin/bash

## Check if TAG was passed
# if [ -z "$1" ]
# then
#       echo "ERROR: First argument for TAG is empty"
#       exit
# fi

## Go to root of project
cd $(dirname $0)
cd ../../
DIR=$(pwd)

COMMIT_HASH=$(git rev-parse HEAD | cut -c1-8)

## Build and push image with input tag
docker build -t "sk8er71091/ts-express-mongo:$COMMIT_HASH" .
docker push "sk8er71091/ts-express-mongo:$COMMIT_HASH"

## Print details
echo ""
echo ""
echo "----------------------------------------------------"
echo ">> Version: $COMMIT_HASH"
echo ">> Image: sk8er71091/ts-express-mongo:$COMMIT_HASH"
echo "----------------------------------------------------"
